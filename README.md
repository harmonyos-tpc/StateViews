## State Views for OpenHarmony
Create & Show progress, data or error views, the easy way!

## Usage
Solution 1 : 
local har package integration
1.Add the har package to the lib folder.
2.Add the following code to gradle of the entry:
```gradle
implementation fileTree(dir: 'libs', include: ['.jar', '.har'])
```
Solution 2:
```gradle
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:StateViews:1.0.2'
```

## Screenshot
![stateview.gif](stateview.gif)

## entry运行要求
通过DevEco studio,并下载OpenHarmonySDK
将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）

## Usage

### 1. Available attributes for PageStatus Builder

| Function        | Description  |
| ------------- |:-------------:|
| addState(params) | Create a new state|
| setIconColor(Int) | Set Icon color |
| setIconSize(Int) | Set Icon Size |
| setTextColor(Int) |  set Title and description colors|
| setFontFace(String) | Set Custom font |
| setButtonBackgroundColor(Int) | Set Button Background color|
| setButtonTextColor(String) | Set Button Text color|

### 2. Available attributes for PageStatesView


| Function        | Description  |
| ------------- |:-------------:|
| displayState(String) | Display a state by his tag name|
| hideStates() | Hide all states and display data|
| displayLoadingState() | Display the loading state|
| addCustomState(Intent) | Create a new state only available for the current activity, fragment...|
| setOnStateButtonClicked(View.OnClickListener) |  Click listener for the state button|
| applyGravity(Int) | Set View Gravity |

### 3. Samples
#### Display an Error View

```java
addState(
    "TAG_ERROR",
    "No Connection",
    "Error retrieving information from server.",
    AppCompatResources.getDrawable(this, R.drawable.ic_server_error),
    "Retry"
    );

mStatusPage.displayState("TAG_ERROR");
```

#### Display a "no Data" View

```java
addState(
    "TAG_NO_RESULTS",
    "No Results Found",
    "Unfortunately I could not find any results matching your search",
    AppCompatResources.getDrawable(this, R.drawable.search), null
)

mStatusPage.displayState("TAG_NO_RESULTS");
```

## Contribute
Contributions are welcome!

## ProGuard
Nothing to include

## License

~~~
The MIT License (MIT)
Copyright (c) 2018 Mehdi Sakout

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
~~~
