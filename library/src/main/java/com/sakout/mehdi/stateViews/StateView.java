package com.sakout.mehdi.stateViews;

import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.agp.utils.TextTool;
import ohos.app.Context;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by medyo on 7/24/17.
 * This view plays as a wrapper for our different stats: Loading, Error and Normal
 */
public class StateView extends PageFlipper {

    private final String TAG = "PageStatusView";
    /**
     * Data States
     */

    StateViewsBuilder mBuilder;
    LayoutScatter mInflater;
    ClickedListener mOnClickListener;

    public StateView(Context context) {
        super(context);
        setupView(null);
    }

    public StateView(Context context, AttrSet attrs) {
        super(context, attrs);
        setupView(attrs);
    }

    private void setupView(AttrSet attrs) {

        // TODO: Add support of dynamic styles

        handleStates();
    }

    private void handleStates() {

        mInflater = LayoutScatter.getInstance(getContext());

        /**
         * Append new state values
         */

        addComponent(getViewByLayoutId(ResourceTable.Layout_state_loading, STATES.LOADING.name()), 0);

        if (hasPrivateConfiguration()) {
            applyCustomStyle(mBuilder, mBuilder.getStates());
        } else if (StateViewsBuilder.getInstance() != null) {
            applyCustomStyle(StateViewsBuilder.getInstance(), StateViewsBuilder.getInstance().getStates());
        }

        hideStates();

    }

    public void addCustomState(String tag, String title, String description, Element icon, String buttonTitle) {

        StateViewsBuilder builder = mBuilder;
        if (builder == null) {
            builder = StateViewsBuilder.getInstance();
        }

        DirectionalLayout view = (DirectionalLayout) mInflater.parse(ResourceTable.Layout_state_custom, null, false);

        setTitleValue(view, title, builder.getTextColor(), builder.getFont());
        setDescriptionValue(view, description, builder.getTextColor(), builder.getFont());
        setIcon(view, icon, builder.getIconColor(), builder.getIconSize());
        setButtonStyle(view, buttonTitle, builder.getFont(), builder.getButtonBackgroundColor(), builder.getButtonTextColor());

        view.setTag(tag);
        addComponent(view, getChildCount());
    }

    private void applyCustomStyle(StateViewsBuilder builder, List<StateModel> states) {

        /*
         * Load Views
         */

        for (int i = 0; i < states.size(); i++) {
            StateModel state = states.get(i);
            int position = i + 1;
            if (state.getCustom()) {

                if (state.getView() != null) {
                    Component view = state.getView();
                    view.setTag(state.getTag());
                    addComponent(view, position);

                } else if (state.getLayoutId() != null) {
                    Component view = mInflater.parse(state.getLayoutId(), null, false);
                    view.setTag(state.getTag());
                    addComponent(view, position);
                }
            } else {
                DirectionalLayout view = (DirectionalLayout) mInflater.parse(ResourceTable.Layout_state_custom, null, false);

                setTitleValue(view, state.getTitle(), builder.getTextColor(), builder.getFont());
                setDescriptionValue(view, state.getDescription(), builder.getTextColor(), builder.getFont());
                setIcon(view, state.getIcon(), builder.getIconColor(), builder.getIconSize());
                setButtonStyle(view, state.getButtonTitle(), builder.getFont(), builder.getButtonBackgroundColor(), builder.getButtonTextColor());

                view.setTag(state.getTag());
                addComponent(view, position);
            }
        }

    }

    public void applyGravity(Integer gravity) {

        StateViewsBuilder builder = mBuilder;

        if (StateViewsBuilder.getInstance() != null) {
            builder = StateViewsBuilder.getInstance();
        }

        if (builder == null) {
            return;
        }

        List<StateModel> states = builder.getStates();
        String[] inlineStates = new String[states.size()];

        for (int i = 0; i < states.size(); i++) {
            inlineStates[i] = states.get(i).getTag();
        }

        for (int i = 0; i < getChildCount(); i++) {
            Component view = getComponentAt(i);

            if (view.getTag() != null && Arrays.asList(inlineStates).contains(view.getTag().toString())) {

                if (view instanceof DirectionalLayout) {
                    ((DirectionalLayout) view).setAlignment(gravity);
                }

            }
        }


    }

    private Boolean hasPrivateConfiguration() {
        return mBuilder != null;
    }

    public void setPrivateConfiguration(StateViewsBuilder builder) {
        this.mBuilder = builder;
    }

    /**
     * Display the show progress View
     */
    public void displayLoadingState() {
        setCurrentIndex(0);
    }

    public void displayState(String tagName) {

        Boolean found = false;
        for (int i = 0; i < getChildCount(); i++) {
            Component view = getComponentAt(i);

            if (view.getTag() != null && view.getTag().toString().equalsIgnoreCase(tagName)) {
                setCurrentIndex(i);
                found = true;
            }
        }

        if (!found) {
            Logger.getLogger(TAG).warning("Tag name Incorrect or not found");
        }
    }

    public void hideStates() {

        for (int i = 0; i < getChildCount(); i++) {
            if (getComponentAt(i).getTag() == null) {
                setCurrentIndex(i);
            }

        }
    }

    /**
     * Return a view based on layout id
     *
     * @param layout Layout Id
     * @param tag    Layout Tag
     * @return View
     */
    private Component getViewByLayoutId(int layout, String tag) {

        Component view = mInflater.parse(layout, null, false);
        view.setTag(tag);
        view.setVisibility(Component.INVISIBLE);

        return view;
    }

    private void setTitleValue(Component view, String value, Color color, Font typeface) {
        Text mTextView = (Text) view.findComponentById(ResourceTable.Id_state_title);
        mTextView.setTextAlignment(TextAlignment.HORIZONTAL_CENTER);
        setTextColor(mTextView, color);
        setTextFont(mTextView, typeface);
        if (!TextTool.isNullOrEmpty(value)) {
            mTextView.setText(value);
            mTextView.setVisibility(Component.VISIBLE);
        } else {
            mTextView.setVisibility(Component.INVISIBLE);
        }
    }

    private void setTextFont(Text mTextView, Font typeface) {
        if (typeface != null) {
            mTextView.setFont(typeface);
        }
    }

    private void setTextColor(Text textView, Color color) {

        if (color != null) {
            textView.setTextColor(color);
        }
    }

    private void setDescriptionValue(Component view, String value, Color color, Font typeface) {
        Text mTextView = (Text) view.findComponentById(ResourceTable.Id_state_description);
        mTextView.setTextAlignment(TextAlignment.HORIZONTAL_CENTER);
        setTextColor(mTextView, color);
        setTextFont(mTextView, typeface);
        if (!TextTool.isNullOrEmpty(value)) {
            mTextView.setText(value);
            mTextView.setVisibility(Component.VISIBLE);
        } else {
            mTextView.setVisibility(Component.INVISIBLE);
        }
    }

    public void setOnStateButtonClicked(ClickedListener clickListener) {
        mOnClickListener = clickListener;

        for (int i = 0; i < getChildCount(); i++) {
            if (getComponentAt(i).getTag() != null) {

                Component view = getComponentAt(i);

                if (view.findComponentById(ResourceTable.Id_state_button) != null) {
                    Button button = (Button) view.findComponentById(ResourceTable.Id_state_button);
                    button.setClickedListener(mOnClickListener);
                }
            }


        }
    }

    private void setButtonStyle(Component view, String buttonTitle, Font typeface, Integer buttonBackgroundColor, Color buttonTextColor) {
        Button button = (Button) view.findComponentById(ResourceTable.Id_state_button);
        setButtonFont(button, typeface);

        if (buttonBackgroundColor != null) {
            button.setBackground(ResUtil.buildDrawableByColor(buttonBackgroundColor));
        }

        if (buttonTextColor != null) {
            button.setTextColor(buttonTextColor);
        }

        if (!TextTool.isNullOrEmpty(buttonTitle)) {
            button.setText(buttonTitle);
            button.setVisibility(VISIBLE);
            button.setClickedListener(mOnClickListener);
        } else {
            button.setVisibility(Component.INVISIBLE);
        }
    }

    private void setButtonFont(Button button, Font typeface) {
        if (typeface != null) {
            button.setFont(typeface);
        }
    }

    private void setIcon(Component view, Element icon, Integer color, int iconSize) {
        Image mImageView = (Image) view.findComponentById(ResourceTable.Id_state_icon);

        if (color != null) {
            //mImageView.setColorFilter(color, graphics.PorterDuff.Mode.SRC_IN);
        } else {
            /*TypedValue typedValue = new TypedValue();
            getContext().getTheme().resolveAttribute(R.attr.colorAccent, typedValue, true);
            mImageView.setColorFilter(typedValue.data, graphics.PorterDuff.Mode.SRC_IN);*/
        }

        if (iconSize > 0) {
            DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig(iconSize, iconSize);
            mImageView.setLayoutConfig(params);
        }

        if (icon != null) {
            mImageView.setImageElement(icon);
            mImageView.setVisibility(Component.VISIBLE);
        } else {
            mImageView.setVisibility(Component.INVISIBLE);
        }

    }

    public enum STATES {
        LOADING
    }


}
