package com.sakout.mehdi.stateViews;

import ohos.agp.components.Component;
import ohos.agp.components.element.Element;

/**
 * Created by medyo on 11/15/17.
 */

public class StateModel {

    private String tag;
    private String title;
    private String description;
    private Element icon;
    private String buttonTitle;
    private Integer layoutId;
    private Component view;
    private Boolean custom = false;
    private Component.ClickedListener clickListener;

    public StateModel() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Element getIcon() {
        return icon;
    }

    public void setIcon(Element icon) {
        this.icon = icon;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getButtonTitle() {
        return buttonTitle;
    }

    public void setButtonTitle(String buttonTitle) {
        this.buttonTitle = buttonTitle;
    }

    public Integer getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(Integer layoutId) {
        this.layoutId = layoutId;
    }

    public Boolean getCustom() {
        return custom;
    }

    public void setCustom(Boolean custom) {
        this.custom = custom;
    }

    public Component getView() {
        return view;
    }

    public void setView(Component view) {
        this.view = view;
    }

    public Component.ClickedListener getClickListener() {
        return clickListener;
    }

    public void setClickListener(Component.ClickedListener clickListener) {
        this.clickListener = clickListener;
    }
}
