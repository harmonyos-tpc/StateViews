package com.sakout.mehdi.stateViews;


import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by medyo on 11/14/17.
 */

public class StateViewsBuilder {

    private static StateViewsBuilder mInstance;
    private final String TAG = "StateViewsBuilder";
    Context mContext;
    List<StateModel> mStates;
    Color mTextColor;
    Integer mIconSize;
    Integer mIconColor;
    Integer mButtonBackgroundColor;
    Color mButtonTextColor;
    Integer mTitleColor;
    Integer mDescriptionColor;
    Font mFont;

    private StateViewsBuilder(Context context) {
        mStates = new ArrayList<>();
        this.mContext = context;
    }

    public static StateViewsBuilder init(Context context) {
        if (mInstance == null) {
            mInstance = new StateViewsBuilder(context);
        }

        return mInstance;
    }

    public static StateViewsBuilder getInstance() {
        return mInstance;
    }

    public Color getTextColor() {
        return mTextColor;
    }

    public StateViewsBuilder setTextColor(Color color) {
        this.mTextColor = color;
        return this;
    }

    public Context getContext() {
        return mContext;
    }

    public List<StateModel> getStates() {
        return mStates;
    }

    public Integer getIconColor() {
        return this.mIconColor;
    }

    public StateViewsBuilder setIconColor(int iconColor) {
        this.mIconColor = iconColor;
        return this;
    }

    public Integer getIconSize() {
        return this.mIconSize;
    }

    public StateViewsBuilder setIconSize(int iconSize) {
        this.mIconSize = iconSize;
        return this;
    }

    /*
    public StateViewsBuilder addState (String tagName, View view){

        StateModel state = new StateModel();
        state.setView(view);
        state.setTag(tagName);
        state.setCustom(true);

        mStates.add(state);
        return this;
    }
*/
    public StateViewsBuilder addState(String tagName, int layout) {

        StateModel state = new StateModel();
        state.setLayoutId(layout);
        state.setTag(tagName);
        state.setCustom(true);

        mStates.add(state);

        return this;
    }

    public StateViewsBuilder addState(String tag, String title, String description, Element icon, String buttonTitle, Component.ClickedListener clickListener) {

        StateModel state = new StateModel();
        state.setTitle(title);
        state.setDescription(description);
        state.setButtonTitle(buttonTitle);
        state.setClickListener(clickListener);
        state.setIcon(icon);
        state.setTag(tag);
        state.setCustom(false);

        mStates.add(state);

        return this;
    }

    public StateViewsBuilder addState(String tag, String title, String description, Element icon, String buttonTitle) {
        addState(tag, title, description, icon, buttonTitle, null);
        return this;
    }

    public StateViewsBuilder setFontFace(Font font) {
        this.mFont = font;
        return this;
    }

    public Font getFont() {
        return mFont;
    }

    public Integer getButtonBackgroundColor() {
        return this.mButtonBackgroundColor;
    }

    public StateViewsBuilder setButtonBackgroundColor(int buttonBackgroundColor) {
        this.mButtonBackgroundColor = buttonBackgroundColor;
        return this;
    }

    public Color getButtonTextColor() {
        return this.mButtonTextColor;
    }

    public StateViewsBuilder setButtonTextColor(Color textColor) {
        this.mButtonTextColor = textColor;
        return this;
    }

}
