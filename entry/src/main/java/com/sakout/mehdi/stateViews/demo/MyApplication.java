/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sakout.mehdi.stateViews.demo;

import com.sakout.mehdi.stateViews.ResUtil;
import com.sakout.mehdi.stateViews.StateViewsBuilder;
import ohos.aafwk.ability.AbilityPackage;
import ohos.agp.utils.Color;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        StateViewsBuilder
                .init(this)
                .setIconColor(Color.getIntColor("#D2D5DA"))
                .addState("error",
                        "No Connection",
                        "Error retrieving information from server.",
                        ResUtil.getVectorDrawable(this,ResourceTable.Graphic_ic_server_error),
                        "Retry"
                )

                .addState(
                        "archive",
                        "Clear the clutter",
                        "Archived items will be kept here. They'll still show in albums " +
                                "& search results.",
                        ResUtil.getVectorDrawable(this,ResourceTable.Graphic_photos_archive),
                        "LEARN MORE"
                )

                .addState("search",
                        "No Results Found",
                        "Unfortunately I could not find any results matching your search",
                        ResUtil.getVectorDrawable(this,ResourceTable.Graphic_search), null)

                .addState("custom",
                        "Custom State",
                        "This is a custom state, made in 5 seconds",
                        ResUtil.getVectorDrawable(this,ResourceTable.Graphic_fingerprint),
                        "Cool")
                .setButtonBackgroundColor(Color.getIntColor("#317DED"))
                .setButtonTextColor(new Color(Color.getIntColor("#FFFFFF")))
                .setIconSize(450);
    }
}
