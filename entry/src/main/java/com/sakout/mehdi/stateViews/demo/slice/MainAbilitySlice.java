/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sakout.mehdi.stateViews.demo.slice;

import com.sakout.mehdi.stateViews.StateView;
import com.sakout.mehdi.stateViews.demo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.ToastDialog;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    StateView mStatusPage;
    Button mLoading, mError, mCustom;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        mLoading = (Button) findComponentById(ResourceTable.Id_button_loading);
        mError = (Button) findComponentById(ResourceTable.Id_button_error);
        mCustom = (Button) findComponentById(ResourceTable.Id_button_custom);

        mLoading.setClickedListener(this);
        mError.setClickedListener(this);
        mCustom.setClickedListener(this);

        mStatusPage = (StateView) findComponentById(ResourceTable.Id_status_page);
        mStatusPage.setOnStateButtonClicked(new Component.ClickedListener() {
            @Override
            public void onClick(Component view) {
                new ToastDialog(MainAbilitySlice.this).setText("Button Clicked").show();
            }
        });
    }

    @Override
    public void onClick(Component component) {
        if (component.getId() == ResourceTable.Id_button_loading) {
            mStatusPage.displayLoadingState();
        } else if (component.getId() == ResourceTable.Id_button_error) {
            mStatusPage.displayState("error");
        } else if (component.getId() == ResourceTable.Id_button_custom) {
            mStatusPage.displayState("archive");
        }
    }
}
